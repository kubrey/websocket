# Websocket server & client (message pusher) #

## Установка ##

```
"require": {
        "kubrey/websocket": "dev-master"
    },
    "minimum-stability": "dev",
    "repositories":[
        {
            "type":"git",
            "url":"https://bitbucket.org/kubrey/websocket"
        }
    ]
```

## Вызов ##

```
use WebSocket\Server\WsServer;

try {
    $ws = new WsServer(array('host'=>'websocket.org','port'=>8000));
    $ws->listen();
} catch(Exception $e){
    echo $e->getMessage();
}
```

## Отправка сообщения ##

```
use WebSocket\WsMessage;
use Websocket\Client\WsStreamClient;

try {
    $msg = new WsMessage();
    $msg->setCategory('test')->setLevel('custom')->setMessage('text of message');
    $wsc = new WsStreamClient(array('host'=>'websocket.org','port'=>8000));
    $wsc->send($msg);
} catch(Exception $e){
    echo $e->getMessage();
}
```