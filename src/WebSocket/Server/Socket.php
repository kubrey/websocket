<?php

namespace WebSocket\Server;

/**
 * Description of Socket
 *
 * @author kubrey
 */
class Socket {

    protected $connections = array();
    protected $clientData = array();

    public function getConnections() {
        return $this->connections;
    }

    /**
     * 
     * @param resource $connect
     * @return \WebSocket\Server\Socket
     */
    public function addConnection($connect) {
        if ($this->isActive($connect)) {
            $socketHash = $this->createSocketHash($connect);
            $this->connections[$socketHash] = $connect;
            $this->clientData[$socketHash] = array();
            $this->addClientData($connect, 'add', time());
        }
        return $this;
    }

    /**
     * 
     * @param resource $connect
     * @param string $field
     * @param mixed $data
     * @return \WebSocket\Server\Socket 
     */
    public function addClientData($connect, $field, $data) {
        $connectHash = array_search($connect, $this->connections);
        if (isset($this->clientData[$connectHash])) {
            $this->clientData[$connectHash][$field] = $data;
        }
        return $this;
    }

    /**
     * 
     * @param resource $connect
     * @param string $field
     * @return null|mixed
     */
    public function getClientData($connect, $field) {
        $connectHash = array_search($connect, $this->connections);
        if (isset($this->clientData[$connectHash][$field])) {
            return $this->clientData[$connectHash][$field];
        } else {
            return null;
        }
    }

    /**
     * 
     * @param resource $connect
     * @return \WebSocket\Server\Socket
     */
    public function removeConnection($connect) {
        $key = array_search($connect, $this->connections);
        if (in_array($connect, $this->connections) && $key) {
            unset($this->connections[$key]);
        }
        if (isset($this->clientData[$key])) {
            unset($this->clientData[$key]);
        }
        return $this;
    }

    /**
     * 
     * @param resource $connect
     * @param string $msg
     * @return boolean
     */
    public function sendTo($connect, $msg) {
        if ($this->isActive($connect)) {
            return (fwrite($connect, $msg)) ? true : false;
        } else {
            return false;
        }
    }

    /**
     * Закрывает сокет и удаляет его из массива текущих подключений
     * @param resource $connect
     */
    public function close($connect) {
        if (is_resource($connect)) {
            fclose($connect);
        }
        $this->removeConnection($connect);
    }

    /**
     * Возвращает уникальный ключ подключенного сокета
     * @param resource $socket
     * @return string
     */
    protected function createSocketHash($socket) {
        $hash = '';
        for ($iter = 0; $iter < 2; $iter++) {
            $hash.=md5(intval($socket) . (uniqid(mt_rand(), true)));
        }
        return $hash;
    }

    /**
     * 
     * @param resource $socket
     * @return boolean
     */
    public function isActive($socket) {
        return (is_resource($socket) && !feof($socket)) ? true : false;
    }

}
