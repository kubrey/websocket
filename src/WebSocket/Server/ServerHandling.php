<?php

namespace WebSocket\Server;

/**
 * Description of ServerHandling
 *
 * @author kubrey
 */
class ServerHandling {

    protected static $lockDir;
    protected static $lockFile;
    private static $actions = array('start', 'stop', 'restart', 'mem_usage', 'help');
    protected $memUsage = false;

    /**
     * 
     * @param array $params
     */
    public function __construct(array $params = array()) {
        self::$lockFile = uniqid(__CLASS__) . ".pid";
        $debug = debug_backtrace();
        $lastLine = end($debug);
        $fileArr = explode("/", $lastLine['file']);
        $file = end($fileArr);
        self::$lockFile = $file . ".pid";

        self::$lockDir = (isset($params['lockDir']) && is_dir($params['lockDir'])) ? $params['lockDir'] : dirname($lastLine['file']);
    }

    /**
     * Возвращает файл в текущей папке, если она не указана явно
     * @return string
     */
    protected static function getPidFile() {
        if (!self::$lockDir || !self::$lockFile) {
            $debug = debug_backtrace();
            $lastLine = end($debug);

            $fileArr = explode("/", $lastLine['file']);
            $file = end($fileArr);
            self::$lockFile = $file . ".pid";
            $dir = (self::$lockDir ? self::$lockDir . DIRECTORY_SEPARATOR : dirname($lastLine['file']) . DIRECTORY_SEPARATOR);
            return $dir . self::$lockFile;
        } else {
            return self::$lockDir . DIRECTORY_SEPARATOR . self::$lockFile;
        }
    }

    /**
     * 
     * @param boolean $set
     * @return \WebSocket\Server\ServerHandling
     */
    public function setMemUsage($set) {
        $this->memUsage = $set;
        return $this;
    }

    /**
     * Запущен ли скрипт
     * @return boolean
     */
    protected static function isRunning() {
        $pid = self::getPidFile();
        if (!file_exists($pid) || !is_readable($pid)) {
            return false;
        }
        $proc = file_get_contents($pid);
        if (!$proc) {
            return false;
        }
        if (!is_numeric($proc)) {
            return false;
        }
        if (is_dir('/proc/' . $proc)) {
            return true;
        } else {
            unlink($pid); //удаляем pid-файл т.к. процесса нет
            return false;
        }
    }

    /**
     * 
     * @return boolean
     */
    protected function setLockFile() {
        $pid = getmypid();
        return (file_put_contents(self::getPidFile(), $pid) > 0 ? true : false);
    }

    /**
     * 
     * @throws \Exception
     * @return boolean
     */
    public static function stop() {
        if (!self::isRunning()) {
            throw new \Exception("Script is not running, nothing to do");
        }
        if (!is_file(self::getPidFile()) || !is_readable(self::getPidFile())) {
            throw new \Exception('Check file permissions of the folder ' . dirname(self::getPidFile()));
        }
        $pid = file_get_contents(self::getPidFile());
        if (!$pid) {
            throw new \Exception("Failed to get pid");
        }

        $out = null;
        exec("kill " . (int) $pid, $out);
        return (!$out) ? true : false;
    }

    /**
     * 
     * @param array $args
     * @return array
     * @throws Exception
     */
    public static function parseCliArgs($args) {
        $default = array('start', array());
        if (!isset($args[1])) {
            return $default;
        }
        if (!in_array($args[1], self::$actions)) {
            throw new \Exception('Unknown option ' . $args[1]);
        }
        switch ($args[1]) {
            case 'stop':
                return array('stop', array());
            case 'help':
                self::showHelp();
                return array('exit', array());
            case 'start':
                return array('start', self::parseCliOptions($args));
            case 'restart':
                return array('restart', self::parseCliOptions($args));
            default:
                throw new \Exception('Unknown option ' . $args[1]);
        }
    }

    /**
     * 
     * @param array $args
     * @return array 
     */
    private static function parseCliOptions($args) {
        unset($args[0]);
        unset($args[1]);
        $options = array();

        foreach ($args as $ind => $arg) {
            if (strpos($arg, '-') === 0 && isset($args[$ind + 1])) {
                $options[substr($arg, 1)] = trim($args[$ind + 1]);
            }
        }
        return $options;
    }

    /**
     * 
     */
    public static function showHelp() {
        $help = "Possible action:\n"
                . "\t start\n"
                . "\t stop\n"
                . "\t restart\n\n"
                . "\t bg-start\n"
                . "\t bg-stop\n"
                . "\t bg-restart\n"
                . "bg-* commands run in background(you can close your terminal wighout killing process)\n\n"
                . "Options for start and restart:\n"
                . "\t -port\n"
                . "\t -host\n\n"
                . "Other options are not required and should be set according to the final server realization, e.g.:\n"
                . "\t -ssl\n"
                . "\t -local_cert\n";
        echo $help;
    }

    protected function writeCurrMemUsage() {
        if ($this->memUsage) {
            $f = self::$lockDir . DIRECTORY_SEPARATOR . str_replace("//", "_", __CLASS__) . "_mem.txt";
            file_put_contents($f, memory_get_usage(true));
        }
    }

}
