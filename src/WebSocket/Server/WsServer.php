<?php

namespace WebSocket\Server;

use WebSocket\WsMessage;
use WebSocket\Server\Socket;

/**
 * Description of WsServer
 * @property Socket $connections
 * @author kubrey
 */
class WsServer extends ServerHandling {

    protected $clients;
    protected $host;
    protected $port;
    protected $null = null;
    protected $socket = null;
    protected $maxMessageSize = 102400;
    protected $params = array();
    protected $events = array();
    protected $connections;

    /**
     * Допустимые параметры:
     * <ul>
     * <li>host* - string</li>
     * <li>port* - int</li>
     * <li>lockDir - string, папка, где будет создан pid файл</li>
     * <li>ssl - boolean</li>
     * <li>local_cert - string, если указан ssl</li>
     * <li>allow_self_signed - boolean, если указан ssl</li>
     * <li>verify_peer - boolean, если указан ssl</li>
     * <li>passphrase - string, если указан ssl</li>
     * </ul>
     * @param array $params
     * @throws \Exception
     */
    public function __construct(array $params) {
        $this->params = $params;
        parent::__construct($params);

        if (self::isRunning()) {
            throw new \Exception('Script is already running');
        }
        $this->host = $params['host'];
        $this->port = $params['port'];
//TCP/IP sream socket
        if (isset($params['ssl']) && $params['ssl'] && isset($params['local_cert'])) {
            $transport = 'ssl';
            $context = $this->getStreamContext();
        } else {
            $transport = 'tcp';
            $context = null;
        }

        $this->socket = stream_socket_server($transport . "://$this->host:$this->port", $errno, $errstr, STREAM_SERVER_BIND | STREAM_SERVER_LISTEN, $context);
        $this->connections = new Socket();
        if (!$this->connections->isActive($this->socket)) {
            throw new \Exception('failed to create socket server:' . $errstr);
        }

        $this->setLockFile();
    }

    /**
     * Закрытие соединения при внезапном прекращении работы( при нормальном завершении соединение будет закрыто в listen)
     */
    public function __destruct() {
        $this->close();
    }

    /**
     * 
     * @return resource
     */
    protected function getStreamContext() {
        $context = stream_context_create();
        stream_context_set_option($context, 'ssl', 'local_cert', $this->params['local_cert']);
        stream_context_set_option($context, 'ssl', 'allow_self_signed', (isset($this->params['allow_self_signed']) ? $this->params['allow_self_signed'] : true));
        stream_context_set_option($context, 'ssl', 'verify_peer', (isset($this->params['verify_peer']) ? $this->params['verify_peer'] : false));
        stream_context_set_option($context, 'ssl', 'passphrase', (isset($this->params['passphrase']) ? $this->params['passphrase'] : null));
        return $context;
    }

    /**
     * 
     * @return Socket
     */
    public function getSocket() {
        return $this->socket;
    }

    /**
     * Подразумевается, что у клиентов есть поле idUser, которое содержит их иденификатор<br>
     * пересылает сообщения указанным клиентам или(если нет адресатов - всем)<br>
     * Для более тонкой обработки входящих сообщений - переопределите метод<br>
     * @param WsMessage $msg
     */
    protected function broadcastMessage(WsMessage $msg) {
        $to = array();
        if ($msg->getAddress()) {
            $to = $msg->getAddress(); //user id
        }
        foreach ($this->connections->getConnections() as $conn) {
            //рассылаем полученное сообщение
            if (!$to) {
                //всем
                $this->connections->sendTo($conn, $this->encode($msg->stringify()));
            } else if (in_array($this->connections->getClientData($conn, 'idUser'), $to)) {
                //тем, кто указан в адресе
                $this->connections->sendTo($conn, $this->encode($msg->stringify()));
            }
        }
    }

    /**
     * 
     * @param string $payload сообщение
     * @param string $type тип сообщения text|close|ping|pong
     * @param boolean $masked
     * @return string закодированная строка
     */
    protected function encode($payload, $type = 'text', $masked = false) {
        $frameHead = array();
        $payloadLength = strlen($payload);

        switch ($type) {
            case 'text':
                // first byte indicates FIN, Text-Frame (10000001):
                $frameHead[0] = 129;
                break;

            case 'close':
                // first byte indicates FIN, Close Frame(10001000):
                $frameHead[0] = 136;
                break;

            case 'ping':
                // first byte indicates FIN, Ping frame (10001001):
                $frameHead[0] = 137;
                break;

            case 'pong':
                // first byte indicates FIN, Pong frame (10001010):
                $frameHead[0] = 138;
                break;
        }

        // set mask and payload length (using 1, 3 or 9 bytes)
        if ($payloadLength > 65535) {
            $payloadLengthBin = str_split(sprintf('%064b', $payloadLength), 8);
            $frameHead[1] = ($masked === true) ? 255 : 127;
            for ($i = 0; $i < 8; $i++) {
                $frameHead[$i + 2] = bindec($payloadLengthBin[$i]);
            }
            // most significant bit MUST be 0
            if ($frameHead[2] > 127) {
                return array('type' => '', 'payload' => '', 'error' => 'frame too large (1004)');
            }
        } elseif ($payloadLength > 125) {
            $payloadLengthBin = str_split(sprintf('%016b', $payloadLength), 8);
            $frameHead[1] = ($masked === true) ? 254 : 126;
            $frameHead[2] = bindec($payloadLengthBin[0]);
            $frameHead[3] = bindec($payloadLengthBin[1]);
        } else {
            $frameHead[1] = ($masked === true) ? $payloadLength + 128 : $payloadLength;
        }

        // convert frame-head to string:
        foreach (array_keys($frameHead) as $i) {
            $frameHead[$i] = chr($frameHead[$i]);
        }
        if ($masked === true) {
            // generate a random mask:
            $mask = array();
            for ($i = 0; $i < 4; $i++) {
                $mask[$i] = chr(rand(0, 255));
            }

            $frameHead = array_merge($frameHead, $mask);
        }
        $frame = implode('', $frameHead);

        // append payload to frame:
        for ($i = 0; $i < $payloadLength; $i++) {
            $frame .= ($masked === true) ? $payload[$i] ^ $mask[$i % 4] : $payload[$i];
        }

        return $frame;
    }

    /**
     * 
     * @param string $data
     * @return boolean|array
     */
    protected function decode($data) {
        $unmaskedPayload = '';
        $decodedData = array();

        // estimate frame type:
        $firstByteBinary = sprintf('%08b', ord($data[0]));
        $secondByteBinary = sprintf('%08b', ord($data[1]));
        $opcode = bindec(substr($firstByteBinary, 4, 4));
        $isMasked = ($secondByteBinary[0] == '1') ? true : false;
        $payloadLength = ord($data[1]) & 127;

        // unmasked frame is received:
        if (!$isMasked) {
            return array('type' => '', 'payload' => '', 'error' => 'protocol error (1002)');
        }

        switch ($opcode) {
            // text frame:
            case 1:
                $decodedData['type'] = 'text';
                break;

            case 2:
                $decodedData['type'] = 'binary';
                break;

            // connection close frame:
            case 8:
                $decodedData['type'] = 'close';
                break;

            // ping frame:
            case 9:
                $decodedData['type'] = 'ping';
                break;

            // pong frame:
            case 10:
                $decodedData['type'] = 'pong';
                break;

            default:
                return array('type' => '', 'payload' => '', 'error' => 'unknown opcode (1003)');
        }

        if ($payloadLength === 126) {
            $mask = substr($data, 4, 4);
            $payloadOffset = 8;
            $dataLength = bindec(sprintf('%08b', ord($data[2])) . sprintf('%08b', ord($data[3]))) + $payloadOffset;
        } elseif ($payloadLength === 127) {
            $mask = substr($data, 10, 4);
            $payloadOffset = 14;
            $tmp = '';
            for ($i = 0; $i < 8; $i++) {
                $tmp .= sprintf('%08b', ord($data[$i + 2]));
            }
            $dataLength = bindec($tmp) + $payloadOffset;
            unset($tmp);
        } else {
            $mask = substr($data, 2, 4);
            $payloadOffset = 6;
            $dataLength = $payloadLength + $payloadOffset;
        }

        /**
         * We have to check for large frames here. socket_recv cuts at 1024 bytes
         * so if websocket-frame is > 1024 bytes we have to wait until whole
         * data is transferd.
         */
        if (strlen($data) < $dataLength) {
            return false;
        }

        if ($isMasked) {
            for ($i = $payloadOffset; $i < $dataLength; $i++) {
                $j = $i - $payloadOffset;
                if (isset($data[$i])) {
                    $unmaskedPayload .= $data[$i] ^ $mask[$j % 4];
                }
            }
            $decodedData['payload'] = $unmaskedPayload;
        } else {
            $payloadOffset = $payloadOffset - 4;
            $decodedData['payload'] = substr($data, $payloadOffset);
        }

        return $decodedData;
    }

    /**
     * handshake с новым клиентом
     * @param resource $connect
     * @return array 
     */
    private function performHandshaking($connect) {
        $info = array();

        $line = fgets($connect);
        $header = explode(' ', $line);
        if (!isset($header[1])) {
            return array();
        }
        $info['method'] = $header[0];
        $info['uri'] = $header[1];

        //считываем заголовки из соединения
        while ($line = rtrim(fgets($connect))) {
            if (preg_match('/\A(\S+): (.*)\z/', $line, $matches)) {
                $info[$matches[1]] = $matches[2];
            } else {
                break;
            }
        }

        $address = explode(':', stream_socket_get_name($connect, true)); //получаем адрес клиента
        $info['ip'] = $address[0];
        $info['port'] = $address[1];

        if (empty($info['Sec-WebSocket-Key'])) {
            return false;
        }

        //отправляем заголовок согласно протоколу вебсокета
        $SecWebSocketAccept = base64_encode(pack('H*', sha1($info['Sec-WebSocket-Key'] . '258EAFA5-E914-47DA-95CA-C5AB0DC85B11')));
        $upgrade = "HTTP/1.1 101 Web Socket Protocol Handshake\r\n" .
                "Upgrade: websocket\r\n" .
                "Connection: Upgrade\r\n" .
                "Sec-WebSocket-Accept:$SecWebSocketAccept\r\n\r\n";
        $this->connections->sendTo($connect, $upgrade);
        $this->connections->addClientData($connect, 'handshakeResponce', $info);

        return $info;
    }

    /**
     * Функция с бесконечным циклом для прослушивания порта
     */
    public function listen() {
        $this->clients = array();
        while (true) {
            //формируем массив прослушиваемых сокетов:
            $read = $this->connections->getConnections();
            $read [] = $this->socket;
            $write = $except = null;

            if (!stream_select($read, $write, $except, null)) {//ожидаем сокеты доступные для чтения (без таймаута)
                break;
            }

            if (in_array($this->socket, $read)) {//есть новое соединение
                //принимаем новое соединение и производим рукопожатие:
                if (($connect = stream_socket_accept($this->socket, -1)) && $info = $this->performHandshaking($connect)) {
                    $this->connections->addConnection($connect);
                    $this->onOpen($connect, $info); //вызываем пользовательский сценарий
                }
                unset($read[array_search($this->socket, $read)]);
            }

            foreach ($read as $connect) {//обрабатываем все соединения
                if (!$this->connections->isActive($connect)) {
                    $this->connections->close($connect);
                    $this->onClose($connect); //вызываем пользовательский сценарий
                    continue;
                }
                $data = $this->read($connect, $this->maxMessageSize);

                $this->handleMessage($connect, $data);
            }
            $this->writeCurrMemUsage();
            $this->onTick();
        }

        $this->close();
    }

    /**
     * 
     * @param resource $connect
     * @param int $size
     * @return string|boolean
     */
    protected function read($connect, $size) {
        return fread($connect, $size);
    }

    /**
     * 
     * @param resource $connect От кого получили сообщение
     * @param string $msg
     * @return boolean|string
     */
    private function handleMessage($connect, $msg) {
        if (!$msg) {
            $this->connections->close($connect);
            $this->onClose($connect); //вызываем пользовательский сценарий
            return 'empty';
        }
        $decodedData = $this->decode($msg);
        if (isset($decodedData['type']) && ($decodedData['type'] == 'close' || !$decodedData['type'])) {
            $this->connections->close($connect);
            $this->onClose($connect); //вызываем пользовательский сценарий
            return 'close';
        }
        if (isset($decodedData['type']) && ($decodedData['type'] == 'text' || $decodedData['type'] == 'binary')) {
            $this->onMessage($connect, $msg); //вызываем пользовательский сценарий
            return $decodedData['type'];
        }
        return false; //invalid frame
    }

    protected function close() {
        if ($this->connections->isActive($this->socket)) {
            fclose($this->socket);
        }
    }

    /**
     * 
     * @return array
     */
    public function getClients() {
        return $this->connections->getConnections();
    }

    /**
     * Колбэк получает в качестве аргумента массив с параметрами: <br>
     * connect - сокет клиента отправителя, data - сообщение после handshake, num_connections - количество подключений,connections - клиенты(массив)
     * @param resource $connect
     * @param array $data handshakeResponce
     */
    private function onOpen($connect, $data) {
        if (isset($this->events['open']) && $this->events['open'] instanceof \Closure) {
            $args = array('connect' => $connect, 'data' => $data, 'num_connects' => count($this->connections->getConnections()), 'connections' => $this->connections);
            $this->events['open']($args);
            unset($args);
        }
        $this->defaultOpenHandling($connect, $data);
        unset($connect, $data);
    }

    /**
     * Событие при получении сообщения<br>
     * По умолчанию рассылает полученное сообщение<br>
     * Колбэк получает в качестве аргумента массив с параметрами: <br>
     * connect - сокет клиента отправителя, data - декодированное сообщение, connections - массив подключений
     * @param resource $connect От кого получили сообщение
     * @param array $data
     */
    private function onMessage($connect, $data) {
        if (isset($this->events['message']) && $this->events['message'] instanceof \Closure) {
            $args = array('connect' => $connect, 'data' => $this->decode($data), 'connections' => $this->connections);
            $this->events['message']($args);
            unset($args);
        }
        $this->defaultMessageHandling($connect, $data);
        unset($connect, $data);
    }

    /**
     * Переопределить для нужной обработки сообщения
     * @param resource $connect От кого получили сообщение
     * @param array $data
     * @return boolean|null
     */
    protected function defaultMessageHandling($connect, $data) {
        $message = $this->decode($data);
        if (!$message) {
            return false;
        }
        $msg = WsMessage::json2msg($message['payload']);
        if (!$msg) {
            return false;
        }

        $this->broadcastMessage($msg);
        unset($message, $msg);
    }

    /**
     * Переопределить для нужной обработки подключения клиента
     * @param resource $connect
     * @param array $data
     */
    protected function defaultOpenHandling($connect, $data) {
        
    }

    /**
     * 
     * @param resource $connect
     */
    protected function defaultCloseHandling($connect) {
        
    }

    /**
     * Колбэк получает в качестве аргумента массив с параметрами: <br>
     * connect - сокет клиента, num_connects - количество подключенных клиентов
     * @param resource $connect
     */
    private function onClose($connect) {
        if (isset($this->events['close']) && $this->events['close'] instanceof \Closure) {
            $args = array('connect' => $connect, 'num_connects' => count($this->connections->getConnections()));
            $this->events['close']($args);
            unset($args);
        }
        $this->defaultCloseHandling($connect);
        unset($connect);
    }

    /**
     * Колбэк получает в качестве аргумента массив с параметрами: <br>
     * num_connects - количество клиентов, memoty_usage - размер используемой памяти, time - текущая метка времени
     * событие нового прохода цикла прослушивания сокета
     */
    protected function onTick() {
        if (isset($this->events['tick']) && $this->events['tick'] instanceof \Closure) {
            $args = array('num_connects' => count($this->connections->getConnections()), 'memory_usage' => memory_get_usage(true), 'time' => time());
            $this->events['tick']($args);
            unset($args);
        }
    }

    /**
     * 
     * @param string $event message|open|close|tick
     * @param Closure $closure
     * @return \WsServer
     */
    public function on($event, $closure) {
        if ($closure instanceof \Closure) {
            $this->events[$event] = $closure;
        }
        return $this;
    }

}
