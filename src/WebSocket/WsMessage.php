<?php

namespace WebSocket;

/**
 * Класс ws-сообщения
 *
 * @author kubrey
 */
class WsMessage {

    protected $level; //уровень ws|backend|etc
    protected $message; //сообщение - массив
    protected $error; //boolean
    protected $category; //string
    protected $address = array(); //кому

    public function __construct() {
        
    }

    public function getLevel() {
        return $this->level;
    }

    public function getCategory() {
        return $this->category;
    }

    public function getMessage() {
        return $this->message;
    }

    public function getError() {
        return $this->error;
    }

    /**
     * 
     * @return array
     */
    public function getAddress() {
        return $this->address;
    }

    /**
     * 
     * @param array $v
     *  @return \WebSocket\WsMessage
     */
    public function setAddress($v) {
        $this->address = $v;
        return $this;
    }

    /**
     * 
     * @param string $v
     * @return \WebSocket\WsMessage
     */
    public function setLevel($v) {
        $this->level = $v;
        return $this;
    }

    /**
     * 
     * @param string $v
     * @return \WebSocket\WsMessage
     */
    public function setCategory($v) {
        $this->category = $v;
        return $this;
    }

    /**
     * 
     * @param mixed $v
     * @return \WebSocket\WsMessage
     */
    public function setMessage($v) {
        $this->message = $v;
        return $this;
    }

    /**
     * 
     * @param boolean $v
     * @return \WebSocket\WsMessage
     */
    public function setError($v) {
        $this->error = $v;
        return $this;
    }

    /**
     * Возвращает json представление текущего сообщения
     * @return string
     */
    public function stringify() {
        $body = array('level' => $this->level, 'message' => $this->message, 'category' => $this->category, 'error' => $this->error, 'address' => $this->address);
        foreach ($body as $k => $v) {
            if ($k == 'error' && !$v) {
                unset($body[$k]);
            }
        }
        return json_encode($body);
    }

    /**
     * 
     * @param string $msg
     * @return \WsMessage
     */
    public static function json2msg($msg) {
        $obj = json_decode($msg);
        $newMsg = new WsMessage();
        if (is_null($obj)) {
            return false;
        }
        foreach ($obj as $key => $val) {
            $newMsg->{$key} = $val;
        }
        return $newMsg;
    }

}
